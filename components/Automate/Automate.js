import {useEffect, useState} from "react";

export const Automate = (props) => {
    const {name, count, img, setter, money, increaseCount} = props

    const [countOfAutomate, setCountOfAutomate] = useState(0)

    const buyAutomate = () => {
        if (money > count * (countOfAutomate + 1) * 4) {
            setCountOfAutomate(countOfAutomate + 1)
            setter(count * (countOfAutomate + 1) * 4)
            increaseCount(count * (countOfAutomate + 1) * 4 * -1)
        }
    }

    return (
        <button onClick={buyAutomate}>{name} за {count * (countOfAutomate + 1) * 4}</button>
    )
}
