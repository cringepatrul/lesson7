import {CustomButton} from "./styles";

const Button = (props) => {
  const { handleClick, value } = props

  return (
    <CustomButton onClick={handleClick}>{value}</CustomButton>
  )
}

export default Button