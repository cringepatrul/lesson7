import styled from 'styled-components'

export const CustomButton = styled.button`
  background-color: greenyellow;
  font-family: "Courier New", Courier, monospace;
  border: none;
  padding: 5px 10px;
  margin: 10px 0;
  font-size: 20px;
  transition: .2s ease-in-out;
  
  &:hover {
    box-shadow: 4px 4px 8px 0px rgba(34, 60, 80, 0.2);
  }
`