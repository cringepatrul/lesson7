import * as S from './../styles/index/styles'
import {useEffect, useState} from "react";
import Button from "../components/Button/Button";
import {Automate} from "../components/Automate/Automate";

const Home = () => {
  const [count, setCount] = useState(0)
  const [multiplier, setMultiplier] = useState(1)
  const [price, setPrice] = useState(multiplier ** 2)
  const [upgradeString, setUpgradeString] = useState(`Улучшить клики до ${multiplier + 1} за ${price}`)
  const [automateCount, setAutomateCount] = useState(0)

  const handleClick = () => {
    setCount(count + multiplier)
  }

  const upgradeClicks = () => {
    if (count >= price) {
      setMultiplier(multiplier + 1)
      setCount(count - price)
      setPrice(multiplier ** 2)
      setUpgradeString(`Улучшить клики до ${multiplier + 1} за ${price}`)
    } else {
      alert(`Денег мало, нужно добыть ${price}`)
    }
  }

  const handleReset = () => {
    setCount(0)
    setMultiplier(1)
    setPrice(multiplier ** 2)
    setUpgradeString(`Улучшить клики до ${multiplier + 1} за ${price}`)
  }

  const addCountFromAutomate = (newAutomateCount) => {
    setAutomateCount(automateCount + newAutomateCount)
  }

  const automateList = [
    {
      id: 0,
      name: '1000-7',
      count: 1
    }, {
      id: 1,
      name: 'Гусь Пётр',
      count: 5
    }, {
      id: 2,
      name: 'Nyan Cat',
      count: 15
    }
  ]

  useEffect(() => {
    const interval = setInterval(() => {

    }, 1000)
    return () => clearInterval(interval)
  })

  return (
    <S.Container>
      <h1>Самый крутой Кликер в мире</h1>
      <h2>{count}</h2>
      <Button handleClick={handleClick} value="Клик!"/>
      <Button handleClick={upgradeClicks} value={upgradeString} />
      <Button handleClick={handleReset} value="Обнулить" />
      {automateList.map(automation => (<Automate name={automation.name} setter={(count) => addCountFromAutomate(count)} money={count} count={automation.count} key={automation.id} increaseCount={(newCount) => setCount(count + newCount)} />))}

    </S.Container>
  )
}

export default Home
