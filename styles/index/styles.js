import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
`

export const Button = styled.button`
  border: none;
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 20px;
  background-color: #c0c0c0;
`

export const MultiplierButton = styled(Button)`
  background-color: aquamarine;
`